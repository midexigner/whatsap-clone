import React, { useState } from 'react'
import './NewChat.css'
import { Avatar } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const NewChat = ({user,chatlist,show,setShow}) => {
    const [list, setList] = useState([
        {id:123,avatar:'https://www.w3schools.com/w3images/avatar1.png',name:"Name"},
        {id:123,avatar:'https://www.w3schools.com/w3images/avatar2.png',name:"Name"},
        {id:123,avatar:'https://www.w3schools.com/w3images/avatar3.png',name:"Name"},
        {id:123,avatar:'https://www.w3schools.com/w3images/avatar4.png',name:"Name"}
    ]);

    const handleClose = ()=>{
        setShow(false)
    }
    return (
        <div className="newChat" style={{left:show?'0px':'-415px'}}>
            <div className="newChat--head">
                <div className="newChat--backButton" onClick={handleClose}>
                <ArrowBackIcon style={{color:'#fff'}}/>
                </div>
                <div className="newChat--headtitle">
                New Conversation
                </div>
            </div>
            <div className="newChat--list">
            {list.map((item,key)=>(
        <div className="newChat--item" key={key}>
             <Avatar src={item.avatar} alt={item.title} className="newChat--itemavatar" />
            <div className="newChat--itemname">{item.name}</div>
        </div>
            ))}
            </div>
        </div>
    )
}

export default NewChat
