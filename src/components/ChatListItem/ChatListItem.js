import React from 'react'
import './ChatListItem.css'
import { Avatar } from '@material-ui/core';

const ChatListItem = ({onClick,item,active}) => {
    return (
        <div 
        className={`chatListItem ${active?'active':''}`}
        onClick={onClick}
        >
           <Avatar src={item.avatar} alt={item.title} className="chatListItem--avatar" />
           <div className="chatListItem--lines">
           <div className="chatListItem--line">
               <div className="chatListItem--name">{item.title}</div>
               <div className="chatListItem--date">19:00</div>
               </div>
           <div className="chatListItem--line">
               <div className="chatListItem--lastMsg"><p>Oops, how are you?</p></div>
               </div>
           </div>
        </div>
    )
}

export default ChatListItem
