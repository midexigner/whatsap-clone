import React,{useState,useEffect,useRef} from 'react'
import './ChatWindow.css'

import { Avatar,TextField  } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import MicIcon from '@material-ui/icons/Mic';
import EmojiPicker from 'emoji-picker-react'
import MessageItem from '../MessageItem/MessageItem';


const ChatWindow = ({user}) => {
    const body = useRef();
    const [emojiOpen,setEmojiOpen] = useState(false);
    const [text, setText] = useState('');
    const [listening,setListening] = useState(false);
    const [list, setList] = useState([
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
        {author:123, body:'bla bla bla'},
        {author:1234, body:'bla bla bla'},
    ]);
useEffect(() => {
    if(body.current.scrollHeight > body.current.offsetHeight){
    body.current.scrollTop =  body.current.scrollHeight - body.current.offsetHeight;
    }
    
}, [list])
    let recognition = null;
    let SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
        if(SpeechRecognition !== undefined) {
            recognition = new SpeechRecognition();
        }

    const handleEmojiClick = (e, emojiObject)=>{
        console.log(emojiObject);
        setText(text + emojiObject.emoji);
    }
    const handleOpenEmoji = () =>{
        setEmojiOpen(true);
    }
    const handleCloseEmoji = () =>{
        setEmojiOpen(false);
    }
    const handleMicClick = () =>{
        if(recognition !==null){
            recognition.onstart = () =>{
                setListening(true)
            }
            recognition.onend = () =>{
                setListening(false)
            }
            recognition.onresult = (e) =>{
                setText(e.results[0][0].transcript );
            }
            recognition.start();
        }
    }
    const handleSendClick = () =>{

    }
    return (
        <div className="chatWindow">
           <div className="chatWindow--header">
           <div className="chatWindow--headerinfo">
                <Avatar src="https://www.w3schools.com/w3images/avatar3.png" alt="avatar" className="chatWindow--avatar" />
                <div className="chatWindow--name">Name here</div>
           </div>
           <div className="chatWindow--headersbuttons">
               <div className="chatWindow--btn"><SearchIcon/></div>
               <div className="chatWindow--btn"><AttachFileIcon/></div>
               <div className="chatWindow--btn"><MoreVertIcon/></div>
           </div>
           </div>
           <div ref={body} className="chatWindow--body">
               {list.map((item,key)=>(
                <MessageItem
                key={key}
                item={item}
                user={user}
                />   
               ))}
           </div>
           <div className="chatWindow--emojiarea"
           style={{height:emojiOpen? '200px' : '0px'}}
           >
               <EmojiPicker 
               disableSearchBar
               disableSkinTonePicker
               onEmojiClick={handleEmojiClick}
                />
           </div>
           <div className="chatWindow--footer">
               <div className="chatWindow--pre">
               <div className="chatWindow--btn" onClick={handleCloseEmoji}
                style={{width:emojiOpen? '40px' : '0px'}}
               ><CloseIcon/></div>
               <div className="chatWindow--btn" onClick={handleOpenEmoji}>
                   <InsertEmoticonIcon
                   style={{color:emojiOpen? '#009688' : '#919191'}}
                   />
                   </div>
               </div>
           <div className="chatWindow--inputarea">  
           <TextField placeholder="Type a message..."
           className="chatWindow--input" 
            variant="outlined"
            value={text}
            onChange={e=>setText(e.target.value)}
            />
  </div>
               <div className="chatWindow--pos">
                   {text === '' && 
                   <div className="chatWindow--btn" onClick={handleMicClick}>
                       <MicIcon
                        style={{color:listening ? '#126ece' : '#919191'}}
                       />
                   </div>
                }
                {text !== '' && 
                   <div className="chatWindow--btn" onClick={handleSendClick}>
                       <SendIcon/>
                   </div>
            }
               </div>
           </div>
        </div>
    )
}

export default ChatWindow
