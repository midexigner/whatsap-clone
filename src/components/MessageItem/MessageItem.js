import React from 'react'
import './MessageItem.css'

const MessageItem = ({item,user}) => {
    return (
        <div className="messageLine" style={{justifyContent:user.id === item.author?'flex-end':'flex-start'}}>
           <div className="messageItem" style={{backgroundColor:user.id === item.author?'#dcf8c6':'#fff'}}>
        <div className="messageText">{item.body}</div>
        <div className="messageDate">18:90</div>
           </div>
        </div>
    )
}

export default MessageItem
