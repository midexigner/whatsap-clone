import React, { useState } from 'react'
import './App.css'
import { Avatar,TextField  } from '@material-ui/core';
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import ChatIcon from '@material-ui/icons/Chat';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SearchIcon from '@material-ui/icons/Search';
import ChatListItem from './components/ChatListItem/ChatListItem';
import ChatIntro from './components/ChatIntro/ChatIntro';
import ChatWindow from './components/ChatWindow/ChatWindow';
import NewChat from './components/NewChat/NewChat';

const App = () => {
  const [chatlist,setChatlist] = useState([
    {chatId:1,title:"name 6",avatar:"https://www.w3schools.com/w3images/avatar6.png"},
    {chatId:2,title:"name 1",avatar:"https://www.w3schools.com/w3images/avatar1.png"},
    {chatId:3,title:"name 5",avatar:"https://www.w3schools.com/w3images/avatar5.png"},
    {chatId:4,title:"name 2",avatar:"https://www.w3schools.com/w3images/avatar2.png"},
    {chatId:5,title:"name 4",avatar:"https://www.w3schools.com/w3images/avatar4.png"},
    {chatId:6,title:"name 3",avatar:"https://www.w3schools.com/w3images/avatar3.png"},
  ])
  const [activeChat,setActiveChat] =useState({})
  const [user,setUser] =useState({
    id:123,
    avatar:'https://www.w3schools.com/howto/img_avatar2.png',
    name:"name 5"
  })
  const [showNewChat,setShowNewChat] = useState(false);
  const handleNewChat = () =>{
    setShowNewChat(true)
  }
  return (
    <div className="app">
      <div className="sidebar">
        <NewChat 
        chatlist={chatlist}
        user={user}
        show={showNewChat}
        setShow={setShowNewChat}
        /> 
<header>
  <Avatar src={user.avatar} alt={user.name} className="header--avatar" />
  <div className="header--buttons">
    <div className="header--btn"><DonutLargeIcon /></div>
    <div className="header--btn" onClick={handleNewChat}><ChatIcon /></div>
    <div className="header--btn"><MoreVertIcon /></div>
  </div>
</header>   
<div className="search">
  <div className="search--input">
  <TextField 
  InputProps={{
    startAdornment: (
        <SearchIcon />
    ),
  }}
  placeholder="search..." 
  variant="outlined" />
  </div>
</div>
<div className="chatlist">
  {chatlist.map((item,key) => (
    <ChatListItem
      key={key}
      item={item}
      active={activeChat.chatId === chatlist[key].chatId }
      onClick={()=>setActiveChat(chatlist[key])}
    />
  ))}
  </div>
      </div>
      <div className="content-area">
        {activeChat.chatId !== undefined &&  
        <ChatWindow
        user={user}
        /> 
        }
        {activeChat.chatId === undefined &&  
        <ChatIntro />
         }
       
      </div>
    </div>
  )
}

export default App
